package test;
import gui.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import bankPack.*;

public class Test {

	public static void main(String[] args) {
		
		
		Bank bank=new Bank();
		Person person=new Person("acu",1);
		Person person1=new Person("ion",2);
		Person person2=new Person("om",3);
		bank.addPerson(person);
		bank.addPerson(person1);
		bank.addPerson(person2);
		Account account=new SpendingAccount(200,1);
		bank.addAssociateAccount(1, account);
		Account account1=new SpendingAccount(300,2);
		bank.addAssociateAccount(1, account1);
		
//		try {
//			bank.readData();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		BankGui gui=new BankGui();
		gui.main(args);
		

	}

}
