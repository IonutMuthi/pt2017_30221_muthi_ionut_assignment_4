package bankPack;
public class SavingAccount extends Account {

	private double interest=0.2;
	
	public SavingAccount(int money, int accountId) {
		super(money, accountId);
	}

	public void deposit(int sum){
		money+=sum+sum*interest;
	}


	
	

}
