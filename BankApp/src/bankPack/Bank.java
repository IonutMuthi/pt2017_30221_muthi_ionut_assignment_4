package bankPack;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;




public class Bank implements BankProc,Serializable {
	Map bank;
	public static ArrayList<Person> persons=new ArrayList();
	public static ArrayList<Account> accounts=new ArrayList();
	
	public Bank(){
		
		 bank=new HashMap<Person,HashSet<Account>>();
			
			try {
				readData();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public Map getHmap(){
		return bank;
	}

	public  ArrayList<Person> getPersons(){
		persons=new ArrayList();
		hashToListP();
		return persons;
	}
	
	public  ArrayList<Account> getAccounts(int id){
		accounts=new ArrayList();
		hashToListA(id);
		
		Person pers=getPersonById(id);
		ArrayList<Account> acc =new ArrayList();
		HashSet<Account> acc1=(HashSet<Account>) bank.get(pers);
		for(Account a:acc){
			acc.add(a);
		}
		System.out.println("am facut");
		return accounts;
	}
	
	private  void hashToListP(){
		Set<Person> setOfKeys=bank.keySet();
		
		for(Person p:setOfKeys){
			persons.add(p);
		}
		System.out.println("person conversion done ");
	}
	
	public  void hashToListA(int id){
		Person pers=getPersonById(id);
		HashSet<Account> acc=(HashSet<Account>) bank.get(pers);
		for(Account a:acc){
			accounts.add(a);
		}
		System.out.println("account conversion done");
	}
	
	public static Account getAccountById(int id){
		Account ac=null;
		for(Account a:accounts){
			if(a.getAccountId()==id)
				ac=a;
		}
		return ac;
	}
	
	public static Person getPersonById(int id){
		Person pers=null;
		for(Person p:persons){
			if(p.getPersonId()==id)
				pers=p;
		}
		return pers;
	}
	
	
	public void updatePerson(int id,Person person){
		Person pers=getPersonById(id);
		HashSet<Account> acc=(HashSet<Account>) bank.get(pers);
		bank.remove(pers);
		bank.put(person, acc);
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void readData() throws IOException{
		FileOutputStream out=new FileOutputStream("bank.ser");
		ObjectOutputStream objOut=new ObjectOutputStream(out);
		objOut.writeObject(bank);
		objOut.close();
	}

	public void writeData() throws IOException, ClassNotFoundException{
		FileInputStream in=new FileInputStream("bank.ser");
		ObjectInputStream objIn=new ObjectInputStream(in);
		bank=(HashMap<Person,HashSet<Account>>) objIn.readObject();
		objIn.close();
		in.close();
		
	}
	
	@Override
	public void addPerson(Person person) {
		assert person!=null;
		int size=bank.size();
		persons.add(person);
		bank.put(person, new HashSet<Account>());
		System.out.println("job done");
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assert size+1==bank.size();
		
		
	}


	@Override
	public void removePerson(Person person) {
		assert !bank.isEmpty();
		int size=bank.size();
		
		persons.remove(person);
		bank.remove(person);
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assert size-1==bank.size();
	
	}


	@Override
	public void addAssociateAccount(int id,Account account) {
		assert account!=null;
		int size=bank.size();
		accounts.add(account);
		Person pers=getPersonById(id);
		HashSet<Account> acc=(HashSet<Account>) bank.get(pers);
		if(acc==null){
			Set<Account> auxAcc=new HashSet<Account>();
			auxAcc.add(account);
			bank.remove(pers);
			bank.put(pers,auxAcc);
		}
		else{
		acc.add(account);
		bank.remove(pers);
		bank.put(pers, acc);
		}
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assert size+1==bank.size();
		
		
	}


	@Override
	public void removeAssociateAccount(int id) {
		assert !bank.isEmpty();
		int size=bank.size();
		Account account=getAccountById(id);
		accounts.remove(account);
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assert size-1==bank.size();
	}



	@Override
	public void readAccountData(Account account) throws IOException {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void writeAccountData(Account account) throws IOException {
		// TODO Auto-generated method stub
		
	}



	
}
