package bankPack;

import java.io.Serializable;
import java.util.Observable;

public class Account  extends Observable implements Serializable{
	protected int money=0;
	protected int accountId;
	
	public Account(int money,int accountId){
		this.money=money;
		this.accountId=accountId;
	}
	
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	
	public	void withdraw(int sum){ 
		setChanged();
		notifyObservers();
		money-=sum;
	}
	
	public void deposit(int sum){
		setChanged();
		notifyObservers();
		money+=sum;
	}

}
