package bankPack;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface BankProc {
	
	/**
	 * @pre person !=null
	 * @post getSize()=getSize()@pre+1
	 */

	 void addPerson(Person person);
	
	/**
	 * @pre !isEmpyy()
	 * @post getSize()=getSize()@pre-1
	 */
	 void removePerson(Person person);
	
	/**
	 * @pre person !=null
	 * @post getSize()=getSize()@pre+1
	 */
	void addAssociateAccount(int id, Account account);
	
	/**
	 * @pre !isEmpyy()
	 * @post getSize()=getSize()@pre-1
	 */
	void removeAssociateAccount(int id);
	
	
	 void readAccountData(Account account) throws  IOException;
	
	
	 void writeAccountData(Account account) throws IOException;

	
	
}
