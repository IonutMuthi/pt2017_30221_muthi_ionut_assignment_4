package bankPack;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable,Observer{
	private String name;
	private int personId;
	
	public Person(String name,int personId){
		this.name=name;
		this.personId=personId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	
	@Override
	public int hashCode(){
		return personId;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Vezi ca ii bai iti fura banii !!");
		
	}
	

}
