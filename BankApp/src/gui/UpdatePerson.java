package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bankPack.*;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UpdatePerson extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdatePerson frame = new UpdatePerson();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdatePerson() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 240, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPesonId = new JLabel("Peson id:");
		lblPesonId.setBounds(21, 31, 78, 14);
		contentPane.add(lblPesonId);
		
		JLabel lblPersonName = new JLabel("Person name: ");
		lblPersonName.setBounds(21, 56, 97, 14);
		contentPane.add(lblPersonName);
		
		textField = new JTextField();
		textField.setBounds(128, 28, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(128, 53, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		Bank bank=new Bank();
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int id=Integer.parseInt(textField.getText());
				Person person=new Person(textField_1.getText(),id);
				bank.updatePerson(id, person);
				
			}
		});
		btnUpdate.setBounds(69, 121, 89, 23);
		contentPane.add(btnUpdate);
	}
}
