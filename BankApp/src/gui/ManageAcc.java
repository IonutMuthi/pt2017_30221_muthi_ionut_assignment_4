package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bankPack.Account;
import bankPack.Bank;
import bankPack.SpendingAccount;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManageAcc extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	public Bank bank=new Bank();
	private JButton btnWithraw;
	private JButton btnDeposit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageAcc frame = new ManageAcc();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageAcc() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 260, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblAccountId = new JLabel("Account id: ");
		lblAccountId.setBounds(10, 11, 71, 14);
		contentPane.add(lblAccountId);
		
		JLabel lblMoney = new JLabel("Sum: ");
		lblMoney.setBounds(10, 55, 57, 14);
		contentPane.add(lblMoney);
		
		textField = new JTextField();
		textField.setBounds(93, 8, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(93, 52, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		btnWithraw = new JButton("Withraw");
		btnWithraw.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int accId=Integer.parseInt(textField.getText());
				int sum=Integer.parseInt(textField_1.getText());
				Account acc=bank.getAccountById(accId);
				acc.withdraw(sum);
				
			}
		});
		btnWithraw.setBounds(10, 100, 89, 23);
		contentPane.add(btnWithraw);
		
		btnDeposit = new JButton("Deposit");
		btnDeposit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int accId=Integer.parseInt(textField.getText());
				int sum=Integer.parseInt(textField_1.getText());
				Account acc=bank.getAccountById(accId);
				acc.deposit(sum);
			}
		});
		btnDeposit.setBounds(120, 100, 89, 23);
		contentPane.add(btnDeposit);
	}

}
