package gui;
import bankPack.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class AddPerson extends JFrame {

	private JPanel contentPane;
	private JTextField textPersonId;
	private JTextField textName;
	public Bank bank= new Bank();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddPerson frame = new AddPerson();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddPerson() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 210, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("Id");
		lblId.setBounds(10, 30, 46, 14);
		contentPane.add(lblId);
		
		textPersonId = new JTextField();
		textPersonId.setBounds(87, 27, 86, 20);
		contentPane.add(textPersonId);
		textPersonId.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 69, 46, 14);
		contentPane.add(lblName);
		
		textName = new JTextField();
		textName.setBounds(87, 66, 86, 20);
		contentPane.add(textName);
		textName.setColumns(10);
		
		JButton btnAddPerson = new JButton("Add person");
		btnAddPerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Person person=new Person(textName.getText(),Integer.parseInt(textPersonId.getText()));
				bank.addPerson(person);
				
					
				
			}
		});
		btnAddPerson.setBounds(37, 123, 120, 36);
		contentPane.add(btnAddPerson);
	}
}
