package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bankPack.Account;
import bankPack.Bank;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;

public class AccountGui1 extends JFrame {

	private JPanel contentPane;
	private JTable table;
	public Bank bank=new Bank();
	private JTextField textField;
	
	public static int id;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AccountGui1 frame = new AccountGui1(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AccountGui1(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 51, 102));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		String[] cols={"Account id","Money"};
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("Account id");
		model.addColumn("Money");
	
		model.addRow(cols);
		ArrayList<Account> accounts= bank.getAccounts(id); 
		for(Account a: accounts){
			Object[] o={a.getAccountId(),a.getMoney()};
			model.addRow(o);
			
		}
		
		table = new JTable(model);
		table.setBounds(216, 11, 208, 221);
		contentPane.add(table);
		
		textField = new JTextField();
		textField.setBounds(120, 197, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnAddSavingAccount = new JButton("Add saving account");
		btnAddSavingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			 AddSavingAcc saving=new AddSavingAcc();
			 saving.setVisible(true);
			}
		});
		btnAddSavingAccount.setBounds(10, 11, 169, 23);
		contentPane.add(btnAddSavingAccount);
		
		JButton btnDeleteAccount = new JButton("Delete Account");
		
		btnDeleteAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				bank.removeAssociateAccount(Integer.parseInt(textField.getText()));
			}
		});
		btnDeleteAccount.setBounds(10, 155, 125, 23);
		contentPane.add(btnDeleteAccount);
		
		JButton btnManage = new JButton("Manage account");
		btnManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ManageAcc manage=new ManageAcc();
				manage.setVisible(true);
			}
		});
		btnManage.setBounds(10, 85, 169, 23);
		contentPane.add(btnManage);
		
		JLabel lblInputTheClientId = new JLabel("Input the account id:");
		lblInputTheClientId.setForeground(Color.WHITE);
		lblInputTheClientId.setBounds(10, 200, 114, 14);
		contentPane.add(lblInputTheClientId);
		
		JButton btnAddSpendingAccount = new JButton("Add spending account");
		btnAddSpendingAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AddSpendingAcc spending=new AddSpendingAcc();
				spending.setVisible(true);
			}
		});
		btnAddSpendingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAddSpendingAccount.setBounds(10, 51, 169, 23);
		contentPane.add(btnAddSpendingAccount);
		
	
		
	}
}
