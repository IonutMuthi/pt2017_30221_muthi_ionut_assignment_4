package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bankPack.*;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddSavingAcc extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	public Bank bank=new Bank();
	private JLabel lblPersonId;
	private JTextField textField_2;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddSavingAcc frame = new AddSavingAcc();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddSavingAcc() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 220, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAccountId = new JLabel("Account id: ");
		lblAccountId.setBounds(10, 11, 71, 14);
		contentPane.add(lblAccountId);
		
		JLabel lblMoney = new JLabel("Money: ");
		lblMoney.setBounds(10, 55, 57, 14);
		contentPane.add(lblMoney);
		
		textField = new JTextField();
		textField.setBounds(93, 8, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(93, 52, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(93, 110, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Account account=new SavingAccount(Integer.parseInt(textField_1.getText()),Integer.parseInt(textField.getText()));
				int id=Integer.parseInt(textField_2.getText());
				bank.addAssociateAccount(id,account);
			}
		});
		btnAdd.setBounds(54, 167, 89, 23);
		contentPane.add(btnAdd);
		
		lblPersonId = new JLabel("Person id:");
		lblPersonId.setBounds(10, 113, 71, 14);
		contentPane.add(lblPersonId);
		
		
	}

}
