package gui;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;

import bankPack.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BankGui {

	private JFrame frame;
	private JTable table;
	
	Bank bank=new Bank();
	public ArrayList<Person> persons=bank.getPersons();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BankGui window = new BankGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BankGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 51, 102));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		String[] cols={"Person id","Name"};
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("Person id");
		model.addColumn("Name");
		model.addRow(cols);
		
		Set<Person> setOfKeys=bank.getHmap().keySet();
		
//		for(Person p:setOfKeys){
//			persons.add(p);
//		}
		for(Person p:persons){
			Object[] o={p.getPersonId(),p.getName()};
			model.addRow(o);
			
		}
		
		table = new JTable(model);
		table.setBounds(216, 11, 208, 221);
		frame.getContentPane().add(table);
		
		JLabel lblInputTheClient = new JLabel("Input the client id:");
		lblInputTheClient.setForeground(Color.WHITE);
		lblInputTheClient.setBounds(10, 200, 114, 14);
		frame.getContentPane().add(lblInputTheClient);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(114, 200, 71, 20);
		frame.getContentPane().add(textPane);
		
		JButton btnAddPerson = new JButton("Add Person");
		btnAddPerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				AddPerson addP=new AddPerson();
				addP.setVisible(true);
			}
		});
		btnAddPerson.setBounds(10, 11, 100, 23);
		frame.getContentPane().add(btnAddPerson);
		
		JButton btnDeletePerson = new JButton("Delete Person");
		btnDeletePerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int id=Integer.parseInt(textPane.getText());
				bank.removePerson(bank.getPersonById(id));
			}
		});
		btnDeletePerson.setBounds(10, 45, 114, 23);
		frame.getContentPane().add(btnDeletePerson);
		
		JButton btnUpdateProduct = new JButton("Update Person");
		btnUpdateProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				UpdatePerson update=new UpdatePerson();
				update.setVisible(true);
				
			}
		});
		btnUpdateProduct.setBounds(10, 85, 125, 23);
		frame.getContentPane().add(btnUpdateProduct);
		
	
		
		JButton btnShowAccounts = new JButton("Show accounts");
		btnShowAccounts.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				AccountGui1 acc=new AccountGui1(Integer.parseInt(textPane.getText()));
				acc.setVisible(true);
			}
		});
		btnShowAccounts.setBounds(10, 133, 131, 23);
		frame.getContentPane().add(btnShowAccounts);
		
		JButton btnRefresh = new JButton("refresh");
		btnRefresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				frame.repaint();
			}
		});
		btnRefresh.setBounds(20, 227, 89, 23);
		frame.getContentPane().add(btnRefresh);
		
		
	}
}
